package view;

import javax.swing.*;

import controller.JSkiRentingPriceController;
import model.JSkiRentingModel;
import model.JSkiRentingModule;

public class JSkiRentingPriceView {

  private JButton calculateBtn;
  private JLabel priceLbl;
  protected JPanel content;
  private JTextArea feesTextArea;
  private JSpinner spinnerHs;
  private JSpinner spinnerMins;
  private JTabbedPane tabbedPane;
  private JTextArea transactionsTextArea;

  private JSkiRentingPriceController parkingPriceController;
  private JSkiRentingModel JSkiRentingModel = JSkiRentingModule.getInstance().getRentingModel();

  private String tickets;

  public JSkiRentingPriceView(JSkiRentingPriceController parkingPriceController) {

    this.parkingPriceController = parkingPriceController;

    initListeners();
    updatePriceField();
  }

  private void initListeners() {
    calculateBtn.addActionListener(e -> requestPrice());

    tabbedPane.addChangeListener(changeEvent -> {
      if(tabbedPane.getSelectedIndex() == 1)
        transactionsTextArea.setText(tickets);
    });

    JSkiRentingModel.addListener(new PriceUpdateListener(this));
  }

  private void updatePriceField() { feesTextArea.setText(JSkiRentingModel.getFormattedFees()); }

  public void updateTickets() { tickets = JSkiRentingModel.getFormattedTickets(); }

  public void updatePriceResult() {
    priceLbl.setText("$" + JSkiRentingModel.lastCalculatedPrice());
  }

  public String getShowedPrice(){ return priceLbl.getText(); }

  private void requestPrice() {

    int hs = Integer.parseInt(spinnerHs.getValue().toString());
    if (hs < 0) {
      spinnerHs.setValue(0);
      hs = 0;
    }

    int mins = Integer.parseInt(spinnerMins.getValue().toString());
    if (mins < 0) {
      spinnerMins.setValue(0);
      mins = 0;
    }

    if (mins >= 60) {
      spinnerMins.setValue(mins % 60);
      mins = mins % 60;
    }

    parkingPriceController.onEventCalculate(hs * 60 + mins);
  }

}
