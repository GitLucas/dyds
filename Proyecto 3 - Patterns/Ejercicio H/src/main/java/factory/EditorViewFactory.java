package main.java.factory;

import main.java.model.Note;
import main.java.model.NotesModel;
import main.java.presenter.NoteEditorPresenter;
import main.java.presenter.NoteEditorPresenterImpl;
import main.java.views.NoteEditorView;
import main.java.views.NoteEditorViewImpl;

import java.util.HashMap;
import java.util.Map;

public class EditorViewFactory {

    private static Map<String, NoteEditorPresenter> createdPresenters = new HashMap<>();


    public static NoteEditorView CreateWithNote(Note note, NotesModel notesModel)
    {
        if(createdPresenters.containsKey(note.getTitle()))
            return OpenAlreadyCreatedWindow(note);

        return OpenNewWindow(note, notesModel);
    }

    private static NoteEditorView OpenAlreadyCreatedWindow(Note note)
    {
        var presenter =  createdPresenters.get(note.getTitle());
        return ValidatedView(presenter);
    }

    private static NoteEditorView OpenNewWindow(Note note, NotesModel notesModel)
    {
        var presenter = CreatePresenter(notesModel);
        presenter.start();
        presenter.updateNoteFields(note);
        createdPresenters.put(note.getTitle(), presenter);
        return presenter.getView();
    }

    public static NoteEditorView Create(NotesModel notesModel)
    {
        var presenter = CreatePresenter(notesModel);
        presenter.start();
        return presenter.getView();
    }

    private static NoteEditorPresenterImpl CreatePresenter(NotesModel notesModel)
    {
        var presenter = new NoteEditorPresenterImpl(notesModel);
        presenter.setView(new NoteEditorViewImpl(presenter));
        return presenter;
    }


    private static NoteEditorView ValidatedView(NoteEditorPresenter presenter)
    {
        NoteEditorView view;

        if(presenter.getView() == null)
            presenter.setView(new NoteEditorViewImpl(presenter));

        view = (NoteEditorView) presenter.getView();
        view.getContent().setVisible(true);
        view.getContent().requestFocus();

        return view;
    }

    public static void StoreUpdatedNote(Note note, NoteEditorPresenter presenter)
    {
        createdPresenters.put(note.getTitle(), presenter);
    }

    public static void RemoveStoredNote(Note note) {
        createdPresenters.remove(note.getTitle());
    }

    public static NoteEditorPresenter GetStoredPresenter(String title) { return createdPresenters.get(title); }

}
