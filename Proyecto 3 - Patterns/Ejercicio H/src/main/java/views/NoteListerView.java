package main.java.views;

import javax.swing.*;

public interface NoteListerView extends BaseView {
    void selectNone();
    JList getNoteList();
    JButton getNewNoteButton();
}
