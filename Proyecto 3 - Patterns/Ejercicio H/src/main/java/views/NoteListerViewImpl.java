package main.java.views;

import main.java.presenter.NoteListerPresenter;

import javax.swing.*;
import java.awt.*;

public class NoteListerViewImpl implements NoteListerView {

    private JPanel content;
    private JList notesJList;
    private JButton createNewNoteBtn;

    NoteListerPresenter presenter;

    @Override
    public Container getContent() {
        return content;
    }

    public NoteListerViewImpl(NoteListerPresenter presenter)
    {
        this.presenter = presenter;
    }

    @Override
    public void selectNone(){
        notesJList.clearSelection();
    }

    @Override
    public JList getNoteList() { return notesJList; }

    @Override
    public JButton getNewNoteButton() { return createNewNoteBtn; }

}
