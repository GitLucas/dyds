package model;

import javax.swing.*;

public interface IListener {
	void onWikiSearch(JPopupMenu popupMenu);
	void onPageSearched();
}
