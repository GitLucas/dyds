package main.java.presenter;

import main.java.factory.EditorViewFactory;
import main.java.model.Note;
import main.java.model.NotesModel;
import main.java.views.BaseView;
import main.java.views.NoteEditorView;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DateFormat;
import java.util.Random;

public class NoteEditorPresenterImpl implements NoteEditorPresenter {

    private NotesModel notesModel;
    private NoteEditorView editorView;

    private Note myNote;

    private Thread taskThread;

    public NoteEditorPresenterImpl(NotesModel model)
    {
        notesModel = model;
    }

    @Override
    public void start()
    {
        showDefaultView();
    }

    private void updateFieldsOfStoredNote() {
        var note = notesModel.getLastUpdatedNote();

        EditorViewFactory.StoreUpdatedNote(note, this);

        if(titleWasModified(note))
            EditorViewFactory.RemoveStoredNote(myNote);

        myNote = note;
    }

    private boolean titleWasModified(Note note) { return myNote != null && !myNote.hasSameTitle(note);}

    public void updateNoteFields(Note note) {
        if (note == null) return;

        myNote = note;
        editorView.setNoteTitle(myNote.getTitle());
        editorView.setTextContent(myNote.getTextContent());
        editorView.setUpdateText(DateFormat.getTimeInstance().format(myNote.getLastUpdate()));
    }

    @Override
    public void cleanFields()
    {
        editorView.setNoteTitle("");
        editorView.setTextContent("");
        editorView.setUpdateText("");
    }

    @Override
    public void onUpdateButtonClicked() {
        var title = editorView.getNoteTitle();
        var contentText = editorView.getTextContent();

        onEventUpdate(title, contentText);
    }



    public void showDefaultView()
    {
        JFrame frame = new JFrame("Note Editor");
        frame.setContentPane(editorView.getContent());
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setLocation(new Random().nextInt(500),0);
        frame.setVisible(true);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                editorClosed();
            }
        });
    }

    @Override
    public Note getNote() { return myNote; }

    @Override
    public void setNote(Note note) { myNote = note; }

    @Override
    public void onEventUpdate(String title, String contentText) {
        taskThread = new Thread(() -> {
            editorView.startWaitingStatus();
            notesModel.updateNote(title, contentText);
            updateFieldsOfStoredNote();
            updateNoteFields(notesModel.getLastUpdatedNote());
            editorView.stopWaitingStatus();
        });

        taskThread.start();
    }

    @Override
    public NoteEditorView getView() {
        return editorView;
    }

    @Override
    public void setView(BaseView view) {
        editorView = (NoteEditorView) view;
    }

    @Override
    public NotesModel getModel() {
        return notesModel;
    }

    @Override
    public boolean isActivelyWorking() {
        return taskThread.isAlive();
    }

    public void setView(NoteEditorView view)
    {
        editorView = view;
    }

    public void editorClosed() {
        var title = editorView.getNoteTitle();
        if(Note.isValidTitleForNote(title))
        {
            notesModel.selectNote(title);
            myNote = notesModel.getSelectedNote();
            EditorViewFactory.RemoveStoredNote(myNote);
        }

        editorView = null;
    }

}
