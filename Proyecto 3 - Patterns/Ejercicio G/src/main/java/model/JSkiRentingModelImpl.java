package model;

import Utils.WaitSimulator;
import view.JSkiModelListener;
import fees.FeeCalculator;
import view.PriceUpdateListener;


import java.util.ArrayList;

class JSkiRentingModelImpl implements JSkiRentingModel {

    private ArrayList<Ticket> tickets = new ArrayList<Ticket>();
    private FeeCalculator feeCalculator = FeeCalculator.getInstance();

    private ArrayList<JSkiModelListener> listeners;

    private float lastPrice;

    JSkiRentingModelImpl()
    {
        listeners = new ArrayList<>();
    }

    @Override public float lastCalculatedPrice() { return lastPrice;}

    @Override public void addListener(PriceUpdateListener listener) { listeners.add(listener);  }

    @Override public void calculatePrice(int minutes) {

        float price = feeCalculator.getFinalPrice(minutes);
        simulatedStoreinRepo(new Ticket(price, minutes));
        lastPrice = price;

        for (var listener : listeners)
            listener.didUpdateParkingPrice();
    }

    private void simulatedStoreinRepo(Ticket ticket){
        //Simulates the times it takes to store this in an external repo!!!
        new Thread(() -> {
            WaitSimulator.simulateLongWait();
            tickets.add(ticket);
        }).start();

        for (var listener : listeners)
            listener.didUpdateTickets();
    }

    @Override public String getFormattedFees() {
        return feeCalculator.getFeeString();
    }

    @Override
    public String getFormattedTickets() {
        String formattedTickets = "";
        for(Ticket ticket: tickets)
            formattedTickets += ticket.id + ": " + ticket.totalPrice + "$, " + ticket.minutesUsed + "mins\n";
        return formattedTickets;
    }

}
