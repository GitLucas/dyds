package tests.java;

import main.java.factory.EditorViewFactory;
import main.java.model.NotesModel;
import main.java.model.NotesModelImpl;
import main.java.model.repository.NonPersistentNotesRepository;
import main.java.presenter.*;
import main.java.utils.WaitSimulator;
import main.java.views.NoteEditorView;
import main.java.views.NoteEditorViewImpl;
import main.java.views.NoteListerViewImpl;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.util.Date;


import static org.junit.Assert.assertEquals;

public class EjGTests {

    NotesModel model;

    Date fixedDate;

    @Before
    public void setUp() throws Exception {
        WaitSimulator.timeBase = 0;

        model = new NotesModelImpl();
        fixedDate = new Date();
        model.setDateManager(new StubbedDateManager(fixedDate));
        model.setNotesRepository(new NonPersistentNotesRepository());
    }

    private NoteListerPresenter createListerPresenter()
    {
        var listerPresenter = new NoteListerPresenterImpl(model);
        listerPresenter.setView(new NoteListerViewImpl(listerPresenter));
        return listerPresenter;
    }

    private NoteEditorPresenter createEditorPresenter()
    {
        var editorPresenter = new NoteEditorPresenterImpl(model);
        editorPresenter.setView(new NoteEditorViewImpl(editorPresenter));
        return editorPresenter;
    }

    @Test(timeout = 500)
    public void testSimpleStorage() throws InterruptedException {
        var editorPresenter = createEditorPresenter();
        editorPresenter.onEventUpdate("Notin", "ouch!");
        waitForPresenterTask(editorPresenter);
        assertEquals(DateFormat.getTimeInstance().format(fixedDate), ((NoteEditorView)editorPresenter.getView()).getUpdateText());
    }

    @Test(timeout = 500)
    public void testSimpleUpdate() throws InterruptedException {
        var editorPresenter = createEditorPresenter();

        editorPresenter.onEventUpdate("Nota 1", "da text");
        waitForPresenterTask(editorPresenter);
        fixedDate = new Date(1);
        model.setDateManager(new StubbedDateManager(fixedDate));
        editorPresenter.onEventUpdate("Nota 1", "da text was changed");
        waitForPresenterTask(editorPresenter);
        assertEquals(DateFormat.getTimeInstance().format(fixedDate), ((NoteEditorView)editorPresenter.getView()).getUpdateText());
    }


    @Test(timeout = 500)
    public void testShowSelection() throws InterruptedException {
        var editorPresenter = createEditorPresenter();
        var listerPresenter = createListerPresenter();

        editorPresenter.onEventUpdate("No me elijas!", "lo hiciste :(");
        waitForPresenterTask(editorPresenter);

        listerPresenter.onEventSelectedNoteTitle("No me elijas!");
        waitForPresenterTask(listerPresenter);

        var noteEditorView = (NoteEditorView) EditorViewFactory.GetStoredPresenter("No me elijas!").getView();

        assertEquals(DateFormat.getTimeInstance().format(fixedDate),
                noteEditorView.getUpdateText());
    }

    @Test(timeout = 500)
    public void testShowUpdateAndSelectOld() throws InterruptedException {
        var editor1 = createEditorPresenter();
        var editor2 = createEditorPresenter();

        var lister = createListerPresenter();

        editor1.onEventUpdate("No me elijas!", "lo hiciste :(");
        waitForPresenterTask(editor1);
        editor2.onEventUpdate("Elegime!", "por favor :D");
        waitForPresenterTask(editor2);
        lister.onEventSelectedNoteTitle("No me elijas!");
        waitForPresenterTask(lister);

        var view = (NoteEditorView) EditorViewFactory.GetStoredPresenter("No me elijas!").getView();

        assertEquals("lo hiciste :(", view.getTextContent());
    }

    @Test(timeout = 500)
    public void testShowSelectUpdatedNoteAfterAdditions() throws InterruptedException {
        var editor1 = createEditorPresenter();
        var editor2 = createEditorPresenter();
        var editor3 = createEditorPresenter();
        var editor4 = createEditorPresenter();
        var lister = createListerPresenter();

        Date oldDate = fixedDate;
        editor1.onEventUpdate("No me cambies", "porfis");
        waitForPresenterTask(editor1);
        editor2.onEventUpdate("Nota del Medio", "Gutierrez");
        waitForPresenterTask(editor2);
        fixedDate = new Date(1);
        model.setDateManager(new StubbedDateManager(fixedDate));
        editor3.onEventUpdate("No me cambies", "lo hiciste nomas...");
        waitForPresenterTask(editor3);
        editor4.onEventUpdate("Nota del Final", "Gutierrez");
        waitForPresenterTask(editor4);
        lister.onEventSelectedNoteTitle("No me cambies");
        waitForPresenterTask(lister);

        var view = (NoteEditorView) EditorViewFactory.GetStoredPresenter("No me cambies").getView();
        assertEquals(DateFormat.getTimeInstance().format(fixedDate), view.getUpdateText());
    }

    private void waitForPresenterTask(NotesPresenter presenter) throws InterruptedException{
        while(presenter.isActivelyWorking()) Thread.sleep(1);
    }

}
