package main.java.views;

import main.java.presenter.NoteEditorPresenter;

import javax.swing.*;
import java.awt.*;

public class NoteEditorViewImpl implements NoteEditorView {
    private JTextField noteTitleTF;
    private JButton updateBtn;
    private JLabel lastUpdateLbl;
    protected JPanel content;
    private JPanel updateIconCard;
    private JLabel stoppedLoadingIcon;
    private JLabel runningLoadingIcon;
    private JPanel mainPanel;
    private JTextPane contentTextTP;

    private NoteEditorPresenter presenter;

    public NoteEditorViewImpl()
    {
        lastUpdateLbl.setText("");
    }

    public NoteEditorViewImpl(NoteEditorPresenter presenter){
        this();
        setPresenter(presenter);
    }


    @Override public void startWaitingStatus() {
        ((CardLayout) updateIconCard.getLayout()).show(updateIconCard, "Running");
        for (Component c: mainPanel.getComponents())
            c.setEnabled(false);
    }

    @Override public void stopWaitingStatus() {
        ((CardLayout) updateIconCard.getLayout()).show(updateIconCard, "Stopped");
        for (Component c: mainPanel.getComponents())
            c.setEnabled(true);
    }

    @Override
    public void setPresenter(NoteEditorPresenter presenter) {
        this.presenter = presenter;
        updateBtn.addActionListener(actionEvent -> presenter.onUpdateButtonClicked());
    }

    @Override
    public String getNoteTitle() { return noteTitleTF.getText(); }

    @Override
    public void setNoteTitle(String title) { noteTitleTF.setText(title); }

    @Override
    public Container getContent() {
        return this.content;
    }

    @Override
    public String getUpdateText(){ return lastUpdateLbl.getText(); }

    @Override
    public void setUpdateText(String updateText) { lastUpdateLbl.setText(updateText); }

    @Override
    public String getTextContent() {
        return contentTextTP.getText();
    }

    @Override
    public void setTextContent(String content) { contentTextTP.setText(content); }
}
