package api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WikipediaPageAPI {

	@GET("api.php?format=json&action=query&prop=extracts&exintro=1")
	Call<String> getExtractByPageID(@Query("pageids") String term);

	@GET("api.php?format=json&action=query&prop=extracts")
	Call<String> getFullPageByID(@Query("pageids") String term);

}
