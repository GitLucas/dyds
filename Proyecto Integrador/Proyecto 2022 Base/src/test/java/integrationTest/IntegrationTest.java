package integrationTest;

import model.Model;
import org.junit.*;
import database.DataBase;

public class IntegrationTest {

	private ViewExtended  view;

	private PresenterExtended presenter() { return (PresenterExtended) PresenterExtended.Instance(); }

	@Before
	public void setup()
	{
		Model model = new Model();
		view = new ViewExtended();
		presenter().setModel(model);
		presenter().setView(view);
		presenter().forceViewInit();
	}

	private void searchPizza()
	{
		view.setSearchText("Pizzas");
		presenter().searchTextFieldListener();
		presenter().searchResultListener(view.getFirstOption());
	}

	@Test
	public void searchTest()
	{
		searchPizza();
		Assert.assertTrue(view.getSearchPaneText().contains("is a dish of Italian origin"));
	}

	@Test
	public void localSaveTest()
	{
		searchPizza();
		presenter().saveLocallyListener();
		Assert.assertTrue(DataBase.getTitles().contains("Pizza"));
	}

	@Test
	public void invalidSearchTest()
	{
		view.setSearchText("njkefiunsciosdosif");
		presenter().searchTextFieldListener();
		Assert.assertEquals(0, view.popup.getComponentCount());
	}

	@Test
	public void deleteFromDatabaseTest()
	{
		var prev = DataBase.getTitles().size();

		if ( prev <= 0 )
		{
			System.out.println("Couldn't test properly.\nTry running localSaveTest() first.");
			return;
		}

		view.selectFirstStoredOption();
		presenter().forceRemoveCurrentSelection();
		Assert.assertEquals(prev - 1, DataBase.getTitles().size());
	}



}
