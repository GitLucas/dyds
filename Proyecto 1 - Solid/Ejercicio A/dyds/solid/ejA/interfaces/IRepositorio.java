package dyds.solid.ejA.interfaces;

import dyds.solid.ejA.Contenido;

public interface IRepositorio {
    public boolean grabar(Contenido contenido);
    public int getRepoSizeForTesting();
}
