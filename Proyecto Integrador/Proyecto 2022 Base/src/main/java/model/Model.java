package model;

import api.WikipediaPageAPI;
import api.WikipediaSearchAPI;
import com.google.gson.*;
import database.DataBase;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import utils.Utils;

import javax.swing.*;
import java.io.IOException;
import java.util.*;

public class Model implements IModel {

	private WikipediaSearchAPI searchAPI;
	private WikipediaPageAPI pageAPI;
	private Gson gson;

	protected List<IListener> listeners;

	String selectedResultTitle = null;
	String lastSearchedText = "";

	@Override public void addListener(IListener listener) { listeners.add(listener); }
	@Override public void removeListener(IListener listener) { listeners.remove(listener); }

	@Override
	public void closeDatabase() { DataBase.tryCloseDatabase(); }

	public String getLastSearch() {return lastSearchedText;}
	public String getSelectedTitle() {return selectedResultTitle;}

	public Model() {
		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl("https://en.wikipedia.org/w/")
				.addConverterFactory(ScalarsConverterFactory.create())
				.build();

		searchAPI = retrofit.create(WikipediaSearchAPI.class);
		pageAPI = retrofit.create(WikipediaPageAPI.class);
		gson = new Gson();
		listeners = new ArrayList<>();

		DataBase.loadDatabase();
	}


	@Override
	public String getExtract(String selectedItem) { return DataBase.getExtract(selectedItem); }

	@Override
	public void saveInfo(String title, String extract) { DataBase.saveInfo(title, extract); }

	@Override
	public void deleteEntry(String entry) { DataBase.deleteEntry(entry); }

	@Override
	public Object[] getTitles() { return DataBase.getTitles().stream().sorted().toArray(); }

	@Override
	public void searchInWiki(String searchText) {
		try {
			JsonArray searchResults = createResultArray(searchText);
			JPopupMenu searchResultsMenu = new JPopupMenu("Search Results");

			for (JsonElement searchElement : searchResults)
				addSearchElement(searchElement, searchResultsMenu);

			for (int i = listeners.size() - 1; i >= 0; i--)
				listeners.get(i).onWikiSearch(searchResultsMenu);

		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	private JsonArray createResultArray(String searchText) throws IOException {
		Response<String> response = searchAPI.searchForTerm(searchText + " articletopic:\"food-and-drink\"").execute();

		JsonObject jsonObject = gson.fromJson(response.body(), JsonObject.class);
		JsonObject query = jsonObject.get("query").getAsJsonObject();

		return query.get("search").getAsJsonArray();
	}

	private void addSearchElement(JsonElement searchElement, JPopupMenu searchResultsMenu) {
		JsonObject searchObject = searchElement.getAsJsonObject();
		var searchResult = Utils.CreateSearchResult(searchObject);
		searchResultsMenu.add(searchResult);
	}

	@Override
	public void searchExtract(String title, String pageID) {
		try {
			internalSearch(title, createExtractObject(pageID));
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	@Override
	public void searchFullPage(String title, String pageID) {
		try {
			internalSearch(title, createFullPageObject(pageID));
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	private void internalSearch(String title, JsonObject object) {

		JsonElement extract = object.get("extract");

		if (extract == null)
			lastSearchedText = "No Results";
		else
			updateSearchText(title, extract);

		for (int i = listeners.size() - 1; i >= 0; i--)
			listeners.get(i).onPageSearched();
	}

	private JsonObject createExtractObject(String pageID) throws IOException {
		Response<String> response = pageAPI.getExtractByPageID(pageID).execute();
		return createObjectFromResponse(response);
	}

	private JsonObject createFullPageObject(String pageID) throws IOException {
		Response<String> response = pageAPI.getFullPageByID(pageID).execute();
		return createObjectFromResponse(response);
	}

	private JsonObject createObjectFromResponse(Response<String> response) {
		JsonObject jsonObject = gson.fromJson(response.body(), JsonObject.class);
		JsonObject query = jsonObject.get("query").getAsJsonObject();
		JsonObject pages = query.get("pages").getAsJsonObject();

		Map.Entry<String, JsonElement> firstPage = pages.entrySet().iterator().next();

		return firstPage.getValue().getAsJsonObject();
	}

	private void updateSearchText(String title, JsonElement extract) {
		selectedResultTitle = title;

		lastSearchedText = "<h1>" + title + "</h1>";
		lastSearchedText += extract.getAsString().replace("\\n", "\n");
		lastSearchedText = Utils.textToHtml(lastSearchedText);

		// TODO
		//text+="\n" + "<a href=https://en.wikipedia.org/?curid=" + searchResultPageId +">View Full Article</a>";
	}

}
