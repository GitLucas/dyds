package dyds.solid.ejA;

import dyds.solid.ejA.interfaces.IManejador;

public class ManejadorDeChulogram implements IManejador {

	int cantidadContenidosEnviados = 0;

	@Override
	public void postearContenido(Contenido contenido) {
		//Codigo dummy para hacer los tests
		cantidadContenidosEnviados++;
	}

	@Override
	public int getCantidadContenidosEnviados() {
		return cantidadContenidosEnviados;
	}

	@Override
	public void resetContenidos(){
		//Por ser un singleton tenemos que limpiarlo para el testing
		cantidadContenidosEnviados = 0;
	}
}
