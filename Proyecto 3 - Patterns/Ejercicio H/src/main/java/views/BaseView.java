package main.java.views;

import java.awt.*;

public interface BaseView {
    Container getContent();
}
