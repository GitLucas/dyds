package dyds.solid.ejB;

import java.util.ArrayList;
import java.util.Random;

public class GeneradorGiftcards {

    private int ultimoCodigo;
    private ArrayList<Giftcard> listaGiftcardsOtorgadas;

    public GeneradorGiftcards() {
        ultimoCodigo = 1000;
        listaGiftcardsOtorgadas = new ArrayList<>();
    }

    public Giftcard crearGiftcard(Usuario usuario){
        ultimoCodigo++;
        int codigoGiftcard = (new Random().nextInt(9000) + 1000)*10000 + ultimoCodigo;
        var g = new Giftcard(codigoGiftcard, usuario);
        listaGiftcardsOtorgadas.add(g);
        return g;
    }
}
