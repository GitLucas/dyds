package boot;

import model.IModel;
import model.Model;
import presenter.Presenter;

public class Program {

	public static void main(String[] args) {
		IModel model = new Model();
		Presenter.Instance().setModel(model);
		Presenter.Instance().createView();
	}
}
