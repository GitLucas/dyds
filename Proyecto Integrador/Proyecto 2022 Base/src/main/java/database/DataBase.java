package database;

import presenter.Presenter;
import utils.Utils;

import java.sql.*;
import java.util.ArrayList;

public class DataBase {

	private static Connection connection;
	private static Statement statement;

	public static void loadDatabase() {
		try {
			createConnectionWithStatement();
			statement.executeUpdate("create table if not exists catalog (id INTEGER, title string PRIMARY KEY, extract string, source integer)");
		} catch (SQLException e) {
			printError(e.getMessage());
		}
	}

	public static ArrayList<String> getTitles() {
		ArrayList<String> titles = new ArrayList<>();

		try
		{
			ResultSet resultSet = statement.executeQuery("select * from catalog");
			while(resultSet.next())
				titles.add(resultSet.getString("title"));
		} catch(SQLException e) {
			printError(e.getMessage());
		}

		return titles;
	}

	public static void saveInfo(String title, String extract) {
		try {
			statement.executeUpdate("replace into catalog values(null, '"+ title + "', '"+ extract + "', 1)");
		} catch(SQLException e) {
			printError(e.getMessage());
		}
	}

	public static String getExtract(String title) {

		try {
			ResultSet resultSet = statement.executeQuery("select * from catalog WHERE title = '" + title + "'" );
			resultSet.next();

			return resultSet.getString("extract");
		} catch(SQLException e) {
			printError(e.getMessage());
		}

		return null;
	}

	public static void deleteEntry(String title)
	{
		try {
			statement.executeUpdate("DELETE FROM catalog WHERE title = '" + title + "'" );
		} catch(SQLException e) {
			printError(e.getMessage());
		}
	}

	private static void createConnectionWithStatement() throws SQLException {
		connection = DriverManager.getConnection(Utils.databaseUrl);
		statement = connection.createStatement();
		statement.setQueryTimeout(30);
	}

	public static void tryCloseDatabase() {
		try {
			if(connection != null)
				connection.close();
		} catch(SQLException e) {
			printError(e.getMessage());
		}
	}

	private static void printError(String msg) {
		System.err.println(msg);
		Presenter.Instance().showAlert(msg);
	}
}
