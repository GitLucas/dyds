package dyds.solid.ejA.interfaces;

import dyds.solid.ejA.Contenido;

public interface IManejador {
    void postearContenido(Contenido contenido);
    int getCantidadContenidosEnviados();
    void resetContenidos();
}
