package dyds.act2.parte1;

public class SearchInfo {

    private String searchText;
    private String topic;
    private String searchWiki;

    public SearchInfo(String searchText, String topic, String searchWiki) {
        this.searchText = searchText;
        this.topic = topic;
        this.searchWiki = searchWiki;
    }

    public String getSearchText() {
        return searchText;
    }

    public String getTopic() {
        return topic;
    }

    public String getSearchWiki() {
        return searchWiki;
    }

    public boolean hasValidTopic() { return topic != null && !topic.equals(""); }
}
