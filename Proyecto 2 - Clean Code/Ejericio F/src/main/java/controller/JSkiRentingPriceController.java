package controller;

import model.JSkiRentingModel;
import model.JSkiRentingModule;
import fees.FeeCalculator;
import view.JSkiRentingPriceView;


public class JSkiRentingPriceController implements JSkiRentingPriceUpdateListener {

  private JSkiRentingModel JSkiRentingModel = JSkiRentingModule.getInstance().getParkingModel();
  private JSkiRentingPriceView JSkiRentingPriceView;

  public JSkiRentingPriceController() {
    initFees();
  }

  private void initFees() {
    FeeCalculator.getInstance().addFee(15, 300);
    FeeCalculator.getInstance().addFee(60, 1000);
    FeeCalculator.getInstance().addFee(5 * 60, 4000);
    FeeCalculator.getInstance().addFee(3 * 60, 2500);
  }

  public void onEventCalculate(int minutes) {
    JSkiRentingModel.calculatePrice(this, minutes);
  }

  public void setParkingPriceView(JSkiRentingPriceView JSkiRentingPriceView) {
    this.JSkiRentingPriceView = JSkiRentingPriceView;
  }

  @Override public void didUpdateParkingPrice(float price) {
    JSkiRentingPriceView.updatePriceResult(price);
  }
}
