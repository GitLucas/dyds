package dyds.act2.tests;

import dyds.act2.parte2.*;
import dyds.act2.tests.utils.StubbedLocalSource;
import dyds.act2.tests.utils.StubbedRemoteSource;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class Parte2Test {
	ClubLocalSource localSource;
	ClubManager clubManager;

	@Before
	public void setUp() throws Exception {
		// Arrange.
		localSource = new StubbedLocalSource();
		ClubRemoteSource remoteSource = new StubbedRemoteSource();
		ClubRepository repo = new ClubRepository(localSource, remoteSource);
		clubManager = new ClubManager(repo);
	}

	@Test
	public void testNonInLocalRepo() {
		assertEquals(null, localSource.getClub(1));
	}


	@Test
	public void testLocalRepoFetchAndStore() {
		Club a =  clubManager.getClub(1);
		assertNotEquals(null, localSource.getClub(1));
	}

	@Test
	public void testObjectFetchAndStore() {
		Club a =  clubManager.getClub(1);
		assertNotEquals(null, a);
	}

	@Test
	public void testObjectFieldsFetchAndStore() {
		Club a =  clubManager.getClub(2);
		assertEquals(a.getIdentity().getName(), "Stream Gold");
	}

	@Test
	public void testObjectFieldsFetchAndStore2() {
		Club a =  clubManager.getClub(1);
		assertEquals(a.getIdentity().getName(), "Nariz Seniors");
	}

	@Test
	public void testNonExistingElement() {
		Club b = clubManager.getClub(999);
		assertEquals(null, b);
	}

	@Test
	public void testOldRefetchedCopy() {
		Club oldClub = new Club(2, "Walking Club", "El Cubo", 1900);
		localSource.storeClub(oldClub);
		oldClub.setLocalRepoTimeStamp(0);

		Club c =  clubManager.getClub(2);

		assertEquals("Stream Gold",c.getIdentity().getName());
	}





}
