package presenter;

import utils.SearchResult;
import model.IModel;
import view.IView;

import javax.swing.*;

public interface IPresenter {

	void setModel(IModel model);
	void setView(IView view);

	void onExit();
	void createView();

	void searchTextFieldListener();
	void searchResultListener(SearchResult sr);
	void saveLocallyListener();

	void showAlert(String msg);

	void storageComboBoxListener();

	JPopupMenu createPopupMenu();

	boolean isActivelyWorking();
}
