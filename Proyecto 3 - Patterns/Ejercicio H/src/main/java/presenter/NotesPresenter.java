package main.java.presenter;

import main.java.model.NotesModel;
import main.java.views.BaseView;

public interface NotesPresenter {
    void start();
    BaseView getView();
    void setView(BaseView view);
    NotesModel getModel();
    boolean isActivelyWorking();
}
