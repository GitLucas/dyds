package view;

import presenter.IPresenter;

import javax.swing.*;

public interface IView extends IViewComponentModifier {

	void setWorkingStatus();
	void setWaitingStatus();

	void alertUser(String msg);
	void showPopupMenu(JPopupMenu popup);

	void initListeners(IPresenter presenter);
	void setBrowserSafe();
	void createFrame();

}
