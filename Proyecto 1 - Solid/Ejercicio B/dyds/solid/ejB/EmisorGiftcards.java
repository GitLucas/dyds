package dyds.solid.ejB;

public class EmisorGiftcards {

    public void enviarGiftcardAUsuario(Giftcard giftcard, Usuario usuario)
    {
        SesionQuepasApp sesionQuepasApp =null;
        try {
            sesionQuepasApp = SesionQuepasApp.getInstance();
            String contactoUsuario = usuario.getContacto();
            String mensaje=this.componerMensajeAviso(giftcard);
            sesionQuepasApp.enviarMensaje(new MensajeQuepasApp().to(contactoUsuario).withBody(mensaje));
        } finally {
            if(sesionQuepasApp !=null)
                sesionQuepasApp.cerrar();
        }
    }

    private String componerMensajeAviso(Giftcard giftcard) {
        return "Gracias! por tu compra ganaste una Giftcard! reclamala con: " + giftcard.darCodigo();
    }
}
