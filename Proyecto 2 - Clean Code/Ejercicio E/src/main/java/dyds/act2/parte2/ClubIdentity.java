package dyds.act2.parte2;

import java.awt.*;

public class ClubIdentity {

    private String name;
    private String stadiumName;
    private int foundingYear;
    private Image colours;

    public ClubIdentity(String name, String stadiumName, int foundingYear)
    {
        this.name = name;
        this.stadiumName = stadiumName;
        this.foundingYear = foundingYear;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getStadiumName() {
        return stadiumName;
    }
    public void setStadiumName(String stadiumName) {
        this.stadiumName = stadiumName;
    }
    public int getFoundingYear() {
        return foundingYear;
    }
    public void setFoundingYear(int foundingYear) {
        this.foundingYear = foundingYear;
    }
    public Image getColours() {
        return colours;
    }
    public void setColours(Image colours) {
        this.colours = colours;
    }
}
