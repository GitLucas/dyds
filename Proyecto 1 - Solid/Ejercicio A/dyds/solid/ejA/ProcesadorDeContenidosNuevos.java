package dyds.solid.ejA;

import dyds.solid.ejA.interfaces.IRepositorio;
import dyds.solid.ejA.redes_sociales.ManejadorDeRedes;

public class ProcesadorDeContenidosNuevos {
	IRepositorio repo = new RepositorioDummy();

	public void procesar(Contenido contenidoNuevo) {
		if (contenidoNuevo.validar() && repo.grabar(contenidoNuevo)) {
			ManejadorDeRedes.getInstance().postearContenido(contenidoNuevo);
		}
	}

	public IRepositorio getRepo() { return repo; }
}
