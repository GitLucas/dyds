package main.java.presenter;

import main.java.model.Note;
import main.java.views.NoteEditorView;

public interface NoteEditorPresenter extends NotesPresenter {
    void onUpdateButtonClicked();
    void cleanFields();
    void setView(NoteEditorView view);
    void showDefaultView();
    Note getNote();
    void setNote(Note note);
    void onEventUpdate(String title, String contentText);
}
