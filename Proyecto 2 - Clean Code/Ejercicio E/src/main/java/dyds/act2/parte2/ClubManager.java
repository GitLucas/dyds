package dyds.act2.parte2;

public class ClubManager {

	private final ClubRepository repo;
	
	public ClubManager(ClubRepository repo) {
		this.repo = repo;
	}

	public Club getClub(int id) {
		
		Club club =  repo.getClubLocalSource().getClub(id);

		if(club == null || clubTooOld(club)) {
			// TODO: Replace these lines with calls to parte1 module, when they decide to finish it
			club = repo.getClubRemoteSource().getClub(id);
			repo.getClubLocalSource().storeClub(club);
		}
		
		return club;
	}

	private boolean clubTooOld(Club club) {
		return System.currentTimeMillis() - club.getLocalRepoTimeStamp() > ClubRepository.daysBeforeRefreshingFromRemote * 24 * 60 * 1000;
	}
}
