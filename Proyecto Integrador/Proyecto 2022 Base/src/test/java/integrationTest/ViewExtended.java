package integrationTest;

import utils.SearchResult;
import view.View;

import javax.swing.*;

public class ViewExtended extends View {

	public JPopupMenu popup;

	@Override
	public void showPopupMenu(JPopupMenu popup) { this.popup = popup; }

	public void setSearchText(String text) { searchTextField.setText(text);	}

	public SearchResult getFirstOption() { return (SearchResult) popup.getComponent(0); }

	public void selectFirstStoredOption() { storageComboBox.setSelectedIndex(0); }

}
