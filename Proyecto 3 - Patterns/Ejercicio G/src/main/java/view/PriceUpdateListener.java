package view;

public class PriceUpdateListener implements JSkiModelListener {

    private JSkiRentingPriceView view;

    public PriceUpdateListener(JSkiRentingPriceView view) { this.view = view;}

    @Override public void didUpdateParkingPrice() {
        view.updatePriceResult();
    }

    @Override
    public void didUpdateTickets() { view.updateTickets(); }
}