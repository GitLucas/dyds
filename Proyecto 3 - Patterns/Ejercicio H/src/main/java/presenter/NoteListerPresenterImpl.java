package main.java.presenter;

import main.java.factory.EditorViewFactory;
import main.java.model.NotesModel;
import main.java.model.NotesModelListener;
import main.java.views.BaseView;
import main.java.views.NoteListerView;
import main.java.views.NoteListerViewImpl;

import javax.swing.*;

public class NoteListerPresenterImpl implements NoteListerPresenter {

    private NotesModel notesModel;
    private NoteListerView noteListerView;
    private DefaultListModel<String> notesListInternalModel = new DefaultListModel<>();

    private Thread taskThread;

    public NoteListerPresenterImpl(NotesModel model)
    {
        notesModel = model;
    }

    @Override
    public void start() {
        noteListerView = new NoteListerViewImpl(this);
        initListeners();
        showDefaultView();
    }

    @Override
    public BaseView getView() {
        return noteListerView;
    }

    @Override
    public void setView(BaseView view) {
        noteListerView = (NoteListerView) view;
    }

    @Override
    public NotesModel getModel() {
        return notesModel;
    }

    @Override
    public boolean isActivelyWorking() {
        return taskThread.isAlive();
    }

    public void showDefaultView() {
        JFrame frame = new JFrame("Notes");
        frame.setContentPane(noteListerView.getContent());
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void initListeners() {
        notesModel.addListener(new NotesModelListener() {
            @Override public void didUpdateNote() {
                updateNoteList();
            }
            @Override public void didSelectNote() { }
        });

        var noteList = noteListerView.getNoteList();

        noteList.addListSelectionListener(listSelectionEvent -> {
            int selectedIndex = noteListerView.getNoteList().getSelectedIndex();
            if(selectedIndex >= 0)
                onEventSelectedNoteTitle(notesListInternalModel.elementAt(selectedIndex));
        });

        noteList.setModel(notesListInternalModel);

        noteListerView.getNewNoteButton().addActionListener(actionEvent -> {
            onEventCreateNewNote();
        });
    }

    @Override
    public void onEventSelectedNoteTitle(String title) {
        noteListerView.selectNone();
        taskThread = new Thread(() -> {
            notesModel.selectNote(title);
            var view = EditorViewFactory.CreateWithNote(notesModel.getSelectedNote(), notesModel);
            view.startWaitingStatus();
            view.stopWaitingStatus();
        });
        taskThread.start();
    }

    @Override
    public void onEventCreateNewNote() {
        EditorViewFactory.Create(notesModel);
    }

    private void updateNoteList() {
        String noteTitleToAddOrUpdate = notesModel.getLastUpdatedNote().getTitle();
        notesListInternalModel.removeElement(noteTitleToAddOrUpdate);
        notesListInternalModel.insertElementAt(noteTitleToAddOrUpdate, 0);
    }

}
