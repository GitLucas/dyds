package view;

public interface JSkiModelListener {

  void didUpdateParkingPrice();
  void didUpdateTickets();
}
