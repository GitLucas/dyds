package dyds.solid.ejA.redes_sociales;

import dyds.solid.ejA.Contenido;
import dyds.solid.ejA.ManejadorDeChulogram;
import dyds.solid.ejA.interfaces.IManejador;

import java.util.LinkedList;
import java.util.List;

public class ManejadorDeRedes {
    private static ManejadorDeRedes instance;

    private ManejadorDeRedes() {
        manejadores = new LinkedList<>();
        manejadores.add(new ManejadorDeChulogram());
    }

    public static ManejadorDeRedes getInstance() {
        if (instance == null) {
            instance = new ManejadorDeRedes();
        }
        return instance;
    }

    private List<IManejador> manejadores;

    public void agregarManejador(IManejador elemento) {
        manejadores.add(elemento);
    }

    public void eliminarManejador(IManejador elemento) {
        manejadores.remove(elemento);
    }


    public void postearContenido(Contenido contenido) {
        //Codigo dummy para hacer los tests
        for (IManejador m : manejadores )
            m.postearContenido(contenido);
    }

    public int getCantidadContenidosEnviados() {
        int count = 0;
        for (IManejador m : manejadores )
            count += m.getCantidadContenidosEnviados();
        return count;
    }

    public void resetSingleton(){
        //Por ser un singleton tenemos que limpiarlo para el testing
        for (IManejador m : manejadores ) m.resetContenidos();
    }
}
