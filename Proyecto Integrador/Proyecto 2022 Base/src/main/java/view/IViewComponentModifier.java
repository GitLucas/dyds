package view;

import javax.swing.*;

public interface IViewComponentModifier {

	String getSearchText();

	void setSearchPaneText(String text);
	void setStoragePaneText(String text);
	void setComboBoxModel(ComboBoxModel<Object> model);

	String getStoragePaneText();
	String getSearchPaneText();
	int getSelectedIndex();
	String getSelectedItem();
	boolean searchFullPage();
}
