package presenterTest;

import utils.SearchResult;
import org.junit.*;
import presenter.Presenter;

public class PresenterTest {

	private TestViewStub viewStub;
	private TestModelStub modelStub;

	@Before
	public void setup() {
		modelStub = new TestModelStub();
		viewStub = new TestViewStub();

		Presenter.Instance().setModel(modelStub);
		Presenter.Instance().setView(viewStub);

		TestDatabaseStub.loadDatabase();
	}

	@Test
	public void textFieldTest() throws InterruptedException {
		Presenter.Instance().searchTextFieldListener();

		waitForPresenterTask();

		var result = viewStub.popup.getComponentCount();

		Assert.assertEquals(1, result);
	}

	@Test
	public void searchResultTest() {
		var result = new SearchResult("title", "id", "snippet");
		Presenter.Instance().searchResultListener(result);
		var testResult = viewStub.getSearchPaneText();

		Assert.assertEquals("last searched text", testResult);
	}

	@Test
	public void localSaveTest() {
		Presenter.Instance().saveLocallyListener();

		Assert.assertEquals("last searched text", TestDatabaseStub.getExtract("selected title"));
	}

	@Test
	public void comboBoxListener()
	{
		TestDatabaseStub.saveInfo("title", "extract");
		viewStub.addItem("title");

		Presenter.Instance().storageComboBoxListener();
		var result = viewStub.getStoragePaneText().split(">")[1].split("<")[0];

		Assert.assertEquals("extract", result);
	}


	private void waitForPresenterTask() throws InterruptedException{
		while(Presenter.Instance().isActivelyWorking()) Thread.sleep(1);
	}

}
