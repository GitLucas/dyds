package main.java.views;

import main.java.presenter.NoteEditorPresenter;

public interface NoteEditorView extends BaseView{

  void startWaitingStatus();

  void stopWaitingStatus();

  void setPresenter(NoteEditorPresenter presenter);


  String getUpdateText();
  void setUpdateText(String updateText);

  String getNoteTitle();
  void setNoteTitle(String title);

  String getTextContent();
  void setTextContent(String content);
}
