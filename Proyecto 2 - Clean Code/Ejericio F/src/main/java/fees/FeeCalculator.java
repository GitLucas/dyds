package fees;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class FeeCalculator {

    private List<Fee> fees = new ArrayList<>();
    Calendar calendar = Calendar.getInstance();

    private static FeeCalculator instance;

    public static FeeCalculator getInstance() {
        if (instance == null) {
            instance =  new FeeCalculator();
        }
        return instance;
    }

    private FeeCalculator() { }

    public void resetSingleton() {
        fees.clear();
        calendar = Calendar.getInstance();
    }


    public void setDate(Date date){
        calendar.setTime(date);
    }

    public float getFinalPrice(int minutes) {
        sortFeesDescending();
        float price = getPriceInternal(minutes);
        price = fixOverprice(minutes, price);

        return price;
    }

    private float getPriceInternal(int minutes)
    {
        var price = 0;

        for (Fee fee : fees) {
            if (minutes / fee.getTimeFraction() <= 0) continue;

            int units = minutes / fee.getTimeFraction();
            price += units * fee.getFractionPrice();
            minutes -= units * fee.getTimeFraction();
        }

        return price + leftoverMinutes(minutes);
    }

    private float leftoverMinutes(int minutes)
    {
        float leftover = 0;
        if (minutes > 0)
            leftover += fees.get(fees.size() - 1).getFractionPrice();
        return leftover;
    }

    private float fixOverprice(int minutes, float price)
    {
        sortFeesAscending();

        for (Fee fee : fees)
            if (fee.eligibleForOverprice(minutes))
                price = Math.min(fee.getOverprice(minutes), price);

        return price;
    }


    public void addFee(int minutes, float price) {
        int currentDay = calendar.get(Calendar.DAY_OF_WEEK);

        if (discountDay(currentDay))
            fees.add(new DiscountFee(minutes, price));
        else
            fees.add(new Fee(minutes, price));
    }

    private boolean discountDay(int day) { return day == Calendar.MONDAY; }

    public String getFeeString()
    {
        sortFeesAscending();
        return FeeFormatter.getFeeString(fees);
    }

    private void sortFeesDescending() { fees.sort((o1, o2) -> o2.getTimeFraction() - o1.getTimeFraction()); }

    private void sortFeesAscending() { fees.sort((o1, o2) -> o1.getTimeFraction() - o2.getTimeFraction());  }
}
