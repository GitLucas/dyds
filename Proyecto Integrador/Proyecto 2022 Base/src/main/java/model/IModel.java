package model;

public interface IModel {

	void addListener(IListener listener);
	void removeListener(IListener listener);

	void closeDatabase();

	String getLastSearch();
	String getSelectedTitle();

	String getExtract(String selectedItem);
	void saveInfo(String title, String extract);
	void deleteEntry(String entry);
	Object[] getTitles();

	void searchInWiki(String searchText);
	void searchExtract(String title, String pageID);
	void searchFullPage(String title, String pageID);
}
