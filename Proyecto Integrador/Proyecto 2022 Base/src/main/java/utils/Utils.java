package utils;

import com.google.gson.JsonObject;
import presenter.Presenter;

public class Utils {

	public static String databaseUrl = "jdbc:sqlite:./dictionary.db";

	public static SearchResult CreateSearchResult(JsonObject searchResult)
	{
		String searchResultTitle = searchResult.get("title").getAsString();
		String searchResultPageId = searchResult.get("pageid").getAsString();
		String searchResultSnippet = searchResult.get("snippet").getAsString();

		SearchResult result = new SearchResult(searchResultTitle, searchResultPageId, searchResultSnippet);
		result.addActionListener(actionEvent -> Presenter.Instance().searchResultListener(result));
		return result;
	}

	public static String textToHtml(String text) {
		return new StringBuilder()
				.append("<font face=\"arial\">")
				.append(SQLSafe(text))
				.append("</font>").toString();
	}

	public static String SQLSafe(String text) { return text.replace("'", "`");	}
}
