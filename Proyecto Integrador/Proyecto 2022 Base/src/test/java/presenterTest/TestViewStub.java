package presenterTest;

import view.View;

import javax.swing.*;

public class TestViewStub extends View {


	public JPopupMenu popup;
	public void showPopupMenu(JPopupMenu popup) {
		this.popup = popup;
	}

	public void addItem(String item) {
		storageComboBox.addItem(item);
		storageComboBox.setSelectedItem(item);
	}


}
