package main.java.presenter;

public interface NoteListerPresenter extends NotesPresenter {

    void onEventSelectedNoteTitle(String title);

    void onEventCreateNewNote();
}
