package dyds.solid.ejA;

import dyds.solid.ejA.interfaces.IRepositorio;

import java.util.ArrayList;

//Este repo es de juguete, cambiar a algo posta en otro Sprint!!!
public class RepositorioDummy implements IRepositorio {

	private ArrayList<Contenido> dummyDB = new ArrayList<>();

	@Override
	public boolean grabar(Contenido contenido) {
		//Codigo dummy para el ejericio
		dummyDB.add(contenido);
		return true;
	}

	@Override
	public int getRepoSizeForTesting(){
		return dummyDB.size();
	}

}
