package presenterTest;

import database.DataBase;
import utils.SearchResult;

import java.util.ArrayList;
import java.util.List;

public class TestDatabaseStub extends DataBase {

	private static List<SearchResult> data;

	public static void loadDatabase() {
		data = new ArrayList<>();
	}

	public static ArrayList<String> getTitles() {
		ArrayList<String> titles = new ArrayList<>();
		for(var result : data)
			titles.add(result.title);
		return titles;
	}

	public static void saveInfo(String title, String extract) {
		data.add(new SearchResult(title, String.format("%d", data.size()), extract));
	}

	public static String getExtract(String title) {
		for(var result : data)
			if(result.title.equals(title))
				return result.snippet;
		return null;
	}

	public static void deleteEntry(String title) {
		data.removeIf(result -> result.title.equals(title));
	}
}
