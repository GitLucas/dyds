package view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import presenter.IPresenter;

public class View implements IView {

	protected JPanel contentPane;
	protected JPanel searchPanel;
	protected JPanel storagePanel;

	protected JTextField searchTextField;

	protected JTextPane searchTextPane;
	protected JTextPane storageTextPane;

	protected JButton saveLocallyButton;

	protected JTabbedPane tabbedPane;

	protected JComboBox<Object> storageComboBox;

	protected JCheckBox fullPageCheckBox;

	protected JFrame frame;

	public View() {}

	public void initListeners(IPresenter presenter)
	{
		searchTextField.addActionListener(e -> presenter.searchTextFieldListener());
		storageTextPane.setComponentPopupMenu(presenter.createPopupMenu());

		saveLocallyButton.addActionListener(actionEvent -> presenter.saveLocallyListener());
		storageComboBox.addActionListener(actionEvent -> presenter.storageComboBoxListener());

		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) { presenter.onExit(); }
		});
	}

	public void setBrowserSafe() {
		searchTextPane.setContentType("text/html");
		storageTextPane.setContentType("text/html");
	}

	public void createFrame() {
		frame = new JFrame("Gourmet Catalog");
		frame.setContentPane(contentPane);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}

	public String getSearchText() { return searchTextField.getText(); }

	public void showPopupMenu(JPopupMenu popup) {
		popup.show(searchTextField, searchTextField.getX(), searchTextField.getY());
	}

	public String getSearchPaneText() { return searchTextPane.getText(); }

	public void setSearchPaneText(String text) {
		searchTextPane.setText(text);
		searchTextPane.setCaretPosition(0);
	}

	public void setStoragePaneText(String text) {
		storageTextPane.setText(text);
	}

	public void setComboBoxModel(ComboBoxModel<Object> model) { storageComboBox.setModel(model); }

	public int getSelectedIndex() {	return storageComboBox.getSelectedIndex(); }

	public String getSelectedItem() { return storageComboBox.getSelectedItem().toString(); }

	public boolean searchFullPage() { return fullPageCheckBox.isSelected(); }

	public String getStoragePaneText() { return storageTextPane.getText(); }


	public void setWorkingStatus() { setAllComponents(false); }

	public void setWaitingStatus() { setAllComponents(true); }

	private void setAllComponents(boolean condition) {
		for(Component c: this.searchPanel.getComponents())
			c.setEnabled(condition);
		searchTextPane.setEnabled(condition);
	}

	public void alertUser(String msg) { JOptionPane.showMessageDialog(frame, msg); }

}
