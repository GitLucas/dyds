package fees;

public class DiscountFee extends Fee {

  public DiscountFee(int timeFraction, float fractionPrice) {
    super(timeFraction, fractionPrice);
  }

  @Override public float getFractionPrice() {
    return super.getFractionPrice() * 0.8f;
  }
}
