import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import utils.Utils;

import java.sql.*;

public class DatabaseTest {

	private Connection connection;
	private Statement statement;


	@Before
	public void setup() {

		try {
			connection = DriverManager.getConnection(Utils.databaseUrl);
			statement = connection.createStatement();
			statement.setQueryTimeout(30);
			statement.executeUpdate("create table if not exists catalog (id INTEGER, title string PRIMARY KEY, extract string, source integer)");
		} catch (SQLException e) { e.printStackTrace(); }
	}

	@Test
	public void updateTests() {
		try {
			statement.executeUpdate("drop table if exists person");
			statement.executeUpdate("create table person (id integer, name string)");
			statement.executeUpdate("insert into person values(1, 'leo')");
			statement.executeUpdate("insert into person values(2, 'yui')");
		} catch (SQLException e) { e.printStackTrace(); }
	}

	@Test
	public void selectTest() {
		try {
			ResultSet rs = statement.executeQuery("select * from catalog");
			while (rs.next()) {
				System.out.println("id = " + rs.getInt("id"));
				System.out.println("title = " + rs.getString("title"));
				System.out.println("extract = " + rs.getString("extract"));
				System.out.println("source = " + rs.getString("source"));
			}
		} catch (SQLException e) {e.printStackTrace();}
	}

	@After
	public void close() {
		try {
			if(connection != null)
				connection.close();
		} catch (SQLException e) { e.printStackTrace(); }
	}
}
