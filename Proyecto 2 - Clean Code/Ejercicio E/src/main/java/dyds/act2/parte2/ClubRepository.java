package dyds.act2.parte2;

public class ClubRepository {
	
	public static final int daysBeforeRefreshingFromRemote = 10;
	
	private final ClubLocalSource clubLocalSource;
	private final ClubRemoteSource clubRemoteSource;
	
	public ClubRepository(ClubLocalSource clubLocalSource, ClubRemoteSource clubRemoteSource) {
		this.clubLocalSource = clubLocalSource;
		this.clubRemoteSource = clubRemoteSource;
	}

	public ClubLocalSource getClubLocalSource() {
		return clubLocalSource;
	}

	public ClubRemoteSource getClubRemoteSource() {
		return clubRemoteSource;
	}
}
