package main.java.main;

import main.java.model.NotesModel;
import main.java.model.NotesModelImpl;
import main.java.model.repository.NonPersistentNotesRepository;
import main.java.presenter.NoteListerPresenter;
import main.java.presenter.NoteListerPresenterImpl;
import main.java.utils.CurrentDateManager;

public class Main {

  public static void main(String[] args) {

    NotesModel model = new NotesModelImpl();
    model.setDateManager(new CurrentDateManager());
    model.setNotesRepository(new NonPersistentNotesRepository());

    NoteListerPresenter presenter = new NoteListerPresenterImpl(model);
    presenter.start();
  }






}
