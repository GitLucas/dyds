package fees;

import java.util.List;

public class FeeFormatter {

    public static String getFeeString(List<Fee> fees) {

        StringBuilder feesString = new StringBuilder();

        for (Fee fee : fees) {
            feesString.append(getTimeString(fee.getTimeFraction()))
                    .append(" - $")
                    .append(fee.getFractionPrice())
                    .append("\n");
        }

        return feesString.toString();
    }

    private static String getTimeString(Integer minutes) {

        StringBuilder timeString = new StringBuilder();

        int hours = minutes / 60;
        if (hours > 0) timeString.append(hours).append("hs ");

        int leftoverMinutes = minutes % 60;
        if (leftoverMinutes > 0)
            timeString.append(leftoverMinutes).append("min");

        return timeString.toString();
    }
}
