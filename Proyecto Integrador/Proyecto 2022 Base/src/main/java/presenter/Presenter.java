package presenter;

import utils.SearchResult;
import model.IListener;
import model.IModel;
import utils.Utils;
import view.IView;
import view.View;

import javax.swing.*;
import java.awt.event.ActionListener;

public class Presenter implements IPresenter {

	protected static IPresenter _instance;

	protected Presenter()
	{
		searchTextFieldThread = new Thread(() -> {
			view.setWorkingStatus();
			model.searchInWiki(view.getSearchText());
			view.setWaitingStatus();
		});
	}

	public static IPresenter Instance()
	{
		if(_instance==null)
			_instance = new Presenter();
		return _instance;
	}

	private IModel model;
	private IView view;

	private final Thread searchTextFieldThread;

	public void setModel(IModel model) {
		this.model = model;

		this.model.addListener(new IListener() {
			@Override public void onWikiSearch(JPopupMenu popupMenu) { showPopupMenu(popupMenu); }
			@Override public void onPageSearched() { handlePageSearch(); }
		});
	}

	public void setView(IView view) { this.view = view; }

	@Override
	public void onExit() { model.closeDatabase(); }

	public void createView() {
		view = new View();
		initViewInternal();
	}

	protected void initViewInternal()
	{
		view.setBrowserSafe();
		view.createFrame();
		view.initListeners(this);

		view.setComboBoxModel(new DefaultComboBoxModel<>(getTitles()));
	}

	@Override
	public void searchTextFieldListener() {
		if(searchTextFieldThread.isAlive()) return;

		searchTextFieldThread.run();
	}

	@Override
	public void searchResultListener(SearchResult result) {
		view.setWorkingStatus();

		if(view.searchFullPage())
			model.searchFullPage(result.title, result.pageID);
		else
			model.searchExtract(result.title, result.pageID);

		view.setWaitingStatus();
	}

	@Override
	public void saveLocallyListener() {
		var lastSearch = model.getLastSearch();

		if (lastSearch.equals("") || lastSearch.equals("No Results")) {
			showAlert("No results were found.");
			return;
		}

		saveInfo(SQLSafeTitle(), lastSearch);
		view.setComboBoxModel(new DefaultComboBoxModel<>(getTitles()));
	}

	@Override
	public void showAlert(String msg) { view.alertUser(msg); }

	@Override
	public void storageComboBoxListener() {
		if (wrongSelection()) return;

		var selectedItem = view.getSelectedItem();
		view.setStoragePaneText(Utils.textToHtml(model.getExtract(selectedItem)));
	}

	private String SQLSafeTitle() { return Utils.SQLSafe(model.getSelectedTitle()); }

	@Override
	public JPopupMenu createPopupMenu()
	{
		JPopupMenu storedInfoPopup = new JPopupMenu();
		addMenuItem(storedInfoPopup, "Delete!", actionEvent -> deleteItemListener());
		addMenuItem(storedInfoPopup, "Save Changes!", actionEvent -> saveItemListener());

		return storedInfoPopup;
	}

	@Override
	public boolean isActivelyWorking() { return searchTextFieldThread.isAlive(); }

	private void addMenuItem(JPopupMenu menu, String itemText, ActionListener listener)
	{
		JMenuItem item = new JMenuItem(itemText);
		item.addActionListener(listener);
		menu.add(item);
	}

	protected void saveItemListener() {
		if (wrongSelection()) return;

		saveInfo(Utils.SQLSafe(view.getSelectedItem()), view.getStoragePaneText());
	}

	protected void deleteItemListener() {
		if (wrongSelection()) return;

		deleteEntry(view.getSelectedItem());
		view.setStoragePaneText("");
	}

	private boolean wrongSelection() {
		if (view.getSelectedIndex() >= 0) return false;

		notifyWrongSelection();
		return true;
	}

	private void notifyWrongSelection() {
		view.alertUser("Please select a valid item.");
	}

	public void showPopupMenu(JPopupMenu popup) { view.showPopupMenu(popup); }

	public void handlePageSearch() {
		view.setSearchPaneText(model.getLastSearch());
	}

	protected void saveInfo(String title, String extract) { model.saveInfo(title, extract); }

	protected void deleteEntry(String entry) {
		model.deleteEntry(entry);
		view.setComboBoxModel(new DefaultComboBoxModel<>(getTitles()));
	}

	protected Object[] getTitles() { return model.getTitles(); }
}
