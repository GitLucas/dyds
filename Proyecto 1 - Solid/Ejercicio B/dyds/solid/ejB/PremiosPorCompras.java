package dyds.solid.ejB;

public class PremiosPorCompras {

    private EmisorGiftcards emisor;
    private GeneradorGiftcards generador;

    public void controlarCompra(Compra compra) {
        Usuario usuario = compra.darComprador();
        if (compra.importe() <= 50000 || !usuario.isGold())
            return;

        Giftcard nuevaGiftcard = generador.crearGiftcard(usuario);
        emisor.enviarGiftcardAUsuario(nuevaGiftcard, usuario);
    }
    public PremiosPorCompras() {
        generador = new GeneradorGiftcards();
        emisor = new EmisorGiftcards();
    }
}
