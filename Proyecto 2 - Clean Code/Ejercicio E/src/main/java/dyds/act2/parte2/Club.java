package dyds.act2.parte2;

import java.awt.*;
import java.util.List;

public class Club {
	
	private int id;
	private List<Player> roster;

	private ClubIdentity identity;
	private CurrentSeasonStatistics stats;

	private long localRepoTimeStamp;
	
	public Club(int id, String name, String stadiumName, int foundingYear) {
		this.id = id;
		identity = new ClubIdentity(name, stadiumName, foundingYear);
		stats = new CurrentSeasonStatistics();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public ClubIdentity getIdentity() { return identity; }
	public void setIdentity(ClubIdentity identity) { this.identity = identity; }

	public List<Player> getRoster() {
		return roster;
	}
	public void setRoster(List<Player> roster) {
		this.roster = roster;
	}

	public void addMatchInfo(int goalsFor, int goalsAgainst){
		stats.addMatchInfo(goalsFor, goalsAgainst);
	}

	public long getLocalRepoTimeStamp() {
		return localRepoTimeStamp;
	}

	public void setLocalRepoTimeStamp(long localRepoTimeStamp) {
		this.localRepoTimeStamp = localRepoTimeStamp;
	}
}
