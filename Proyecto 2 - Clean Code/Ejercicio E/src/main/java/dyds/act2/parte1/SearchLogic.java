package dyds.act2.parte1;

import com.google.gson.Gson;

import java.net.URI;
import java.net.URISyntaxException;

public class SearchLogic {

    private ServiceProvider serviceProvider;

    public SearchLogic(ServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    // GET Request to search in Wiki. Format: <base-wiki-url>/w/api.php?<actions>&srsearch=<search information>
    // Example:
    // https://en.wikipedia.org/w/api.php?action=query&format=json&list=search&srsearch=Baldurs%20Gate%203%20articletopic%3A%22video-games%22"
    // Result example: { "result" : "[pageid1, pageid2, pageid3,...]" }
    public String[] searchInWiki(SearchInfo searchInfo) {

        var searchUri = getSearchUri(searchInfo);
        String json = null;

        try {
            json = serviceProvider.resolveCall(new URI(searchUri));
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (json != null) {
            Gson gson = new Gson();
            var searchResult = gson.fromJson(json, SearchResult.class);
            return searchResult.getResult();
        }

        return new String[0];
    }

    private String getSearchUri(SearchInfo searchInfo)
    {
        String rest = "action=query&format=json&list=search";

        String url ="https://"+ searchInfo.getSearchWiki() + "/w/api.php?"+ rest +"&srsearch=" + searchInfo.getSearchText();

        if(searchInfo.hasValidTopic())
            url +=" articletopic%3A\"" + searchInfo.getTopic() + "\"";

        url = url.replace(" ", "%20");
        url = url.replace("\"", "%22");

        return url;
    }

}
