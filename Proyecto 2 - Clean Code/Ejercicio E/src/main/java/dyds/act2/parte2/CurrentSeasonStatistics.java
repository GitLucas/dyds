package dyds.act2.parte2;

public class CurrentSeasonStatistics {
    private int won;
    private int lost;
    private int goalsFor;
    private int goalsAgainst;

    public void addMatchInfo(int goalsFor, int goalsAgainst)
    {
        handleVictory(goalsFor, goalsAgainst);
        this.goalsFor += goalsFor;
        this.goalsAgainst += goalsAgainst;
    }

    private void handleVictory(int goalsFor, int goalsAgainst)
    {
        if(goalsFor > goalsAgainst) won++;
        else lost++;
    }
}
