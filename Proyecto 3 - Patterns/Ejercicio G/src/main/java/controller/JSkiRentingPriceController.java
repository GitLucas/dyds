package controller;

import fees.FeeCalculator;
import model.JSkiRentingModel;
import model.JSkiRentingModule;
import view.JSkiRentingPriceView;


public class JSkiRentingPriceController {

  private JSkiRentingModel JSkiRentingModel = JSkiRentingModule.getInstance().getRentingModel();
  private JSkiRentingPriceView JSkiRentingPriceView;

  public JSkiRentingPriceController() {
    initFees();
  }

  private void initFees() {
    FeeCalculator.getInstance().addFee(15, 300);
    FeeCalculator.getInstance().addFee(60, 1000);
    FeeCalculator.getInstance().addFee(5 * 60, 4000);
    FeeCalculator.getInstance().addFee(3 * 60, 2500);
  }

  public void onEventCalculate(int minutes) {
    JSkiRentingModel.calculatePrice(minutes);
  }

  public void setParkingPriceView(JSkiRentingPriceView JSkiRentingPriceView) {
    this.JSkiRentingPriceView = JSkiRentingPriceView;
  }
}
