package presenterTest;

import utils.SearchResult;
import model.Model;

import javax.swing.*;
import java.util.ArrayList;

public class TestModelStub extends Model {

	public TestModelStub() {
		listeners = new ArrayList<>();
	}

	@Override
	public void closeDatabase() {}

	@Override
	public String getExtract(String selectedItem) { return TestDatabaseStub.getExtract(selectedItem); }

	@Override
	public void saveInfo(String title, String extract) { TestDatabaseStub.saveInfo(title, extract); }

	@Override
	public void deleteEntry(String entry) { TestDatabaseStub.deleteEntry(entry); }

	@Override
	public Object[] getTitles() { return TestDatabaseStub.getTitles().stream().sorted().toArray(); }

	@Override
	public String getLastSearch() {
		return "last searched text";
	}

	@Override
	public String getSelectedTitle() {
		return "selected title";
	}

	@Override
	public void searchInWiki(String searchText) {
		JPopupMenu searchResultsMenu = new JPopupMenu("Search Results");
		searchResultsMenu.add(new SearchResult("title", "id", "snippet"));

		for (int i = listeners.size() - 1; i >= 0; i--)
			listeners.get(i).onWikiSearch(searchResultsMenu);
	}

	@Override public void searchExtract(String title, String pageID) { notifySearch(); }
	@Override public void searchFullPage(String title, String pageID) { notifySearch(); }

	private void notifySearch() {
		for (int i = listeners.size() - 1; i >= 0; i--)
			listeners.get(i).onPageSearched();
	}
}
