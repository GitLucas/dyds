package model;

import view.PriceUpdateListener;

public interface JSkiRentingModel {

  float lastCalculatedPrice();

  public void addListener(PriceUpdateListener listener);

  void calculatePrice(int minutes);

  String getFormattedFees();

  String getFormattedTickets();

}
