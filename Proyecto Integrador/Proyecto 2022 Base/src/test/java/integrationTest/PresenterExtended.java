package integrationTest;

import presenter.IPresenter;
import presenter.Presenter;

public class PresenterExtended extends Presenter {

	public static IPresenter Instance()
	{
		if(_instance==null)
			_instance = new PresenterExtended();
		return _instance;
	}

	public void forceViewInit(){ initViewInternal(); }

	public void forceRemoveCurrentSelection() { deleteItemListener(); }
}
