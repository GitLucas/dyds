package dyds.act2.parte1;

public class SearchResult {

  private String[] result;

  public SearchResult(String[] result) {
    this.result = result;
  }

  public String[] getResult() {
    return result;
  }

  public void setResult(String[] result) {
    this.result = result;
  }


}
