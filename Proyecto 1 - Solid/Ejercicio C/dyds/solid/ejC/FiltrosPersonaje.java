package dyds.solid.ejC;

import java.util.ArrayList;
import java.util.List;

public class FiltrosPersonaje {
	public List<Personaje> where(List<Personaje> personajes, FuncionFiltro filtro)
	{
		var list = new ArrayList<Personaje>();
		for (var p : personajes) if(filtro.aplicarFiltro(p)) list.add(p);
		return list;
	}
}
